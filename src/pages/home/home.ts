import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, App } from 'ionic-angular';
import { Geolocation, GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';
import {Location} from '../../models/location';
import {Course} from '../../models/course';
import { GlobalProvider } from "../../providers/global";
import firebase from "firebase";

declare var google;
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  infoWindow: any;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  options : GeolocationOptions;
  currentPos : Geoposition;
  courses: Array<Course> = [];
  features: Array<Location> = [];
  filters: Array<Location> = [];
  searchQuery: any;
  loadingView: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private geolocation: Geolocation, public loadingCtrl: LoadingController, private storage: Storage, public app: App, public global: GlobalProvider) {
      this.showLoadingView();
  }

  showLoadingView(show = true, desc?: String) {
    if (show) {
      // show showLoading view
      this.loadingView = this.loadingCtrl.create();
      this.loadingView.present();
    }
    else {
      if (this.loadingView) {
        // hide
        this.loadingView.dismiss();
      }
    }
  }

  ionViewDidLoad() {
    this.features = [];
    this.filters = [];
    this.storage.get('current_user').then((val)=> {
      if (val == null) {
        this.app.getRootNav().setRoot('LoginPage');
      } else {
        if (this.global.features.length == 0) {
          this.fetchAllLocations().then((locations) => {
            for (var i = 0; i < 10; i ++) {
              this.global.features.push({
                lat: locations[i].lat,
                lng: locations[i].lng,
                name: locations[i].name,
                price: locations[i].green_fee
              });
            }
            this.features = this.global.features;
            this.filters = this.global.features;
            this.initMap();
          });
        } else {
          this.features = this.global.features;
          this.filters = this.global.features;
          this.initMap();
        }
        this.fetchAllCourses().then((courses) => {
          this.global.courses = courses;
          this.courses = courses;
        });
      }
    });
  }

  // search function
  searchLocations() {
    this.filters = [];
    for (var i = 0; i < 10; i ++) {
      if (this.features[i].name.toLowerCase().match(this.searchQuery.toLowerCase())) {
        this.filters.push(this.features[i]);
      }
    }
    if (this.filters.length > 0) {
      var content = this.filters[0].name + " , lat= " + this.filters[0].lat + " , lng= " + this.filters[0].lng;
      this.addMarkersToMap(this.filters[0].lat, this.filters[0].lng, content);      
      this.map.setCenter(new google.maps.LatLng(this.filters[0].lat, this.filters[0].lng));
      this.map.setZoom(15);
    }
  }

  initMap() {
    this.options = {
      enableHighAccuracy : true
    };

    this.geolocation.getCurrentPosition(this.options).then((pos : Geoposition) => {
      this.currentPos = pos;
      this.map = new google.maps.Map(this.mapElement.nativeElement, {
        zoom: 12,
        center: { lat: this.currentPos.coords.latitude, lng: this.currentPos.coords.longitude },
        streetViewControl: true, //default
        streetViewControlOptions: {
          position: google.maps.ControlPosition.LEFT_TOP
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      var content = "lat= " + this.currentPos.coords.latitude + " , lng= " + this.currentPos.coords.longitude;
      this.addMarkersToMap(this.currentPos.coords.latitude, this.currentPos.coords.longitude, content); 
      
      this.directionsDisplay.setMap(this.map);
      this.infoWindow = new google.maps.InfoWindow();      
            
      this.showLoadingView(false);
    },(err : PositionError)=>{
        console.log("error : " + err.message);
    });
  }

  addMarkersToMap(lat, lng, content) {
    
    this.infoWindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(lat, lng)
    });
    var ref = this;
    google.maps.event.addListener(marker, 'click', function () {
      ref.infoWindow.setContent(content);
      ref.infoWindow.open(this.map, this);
    });
  }

  presentModal() {
    let modal = this.modalCtrl.create('CategoryPage');
    modal.present();
  }

  fetchAllLocations() {
    
    let locations = [];
    const dbRef = firebase.database().ref();

    let query: any = dbRef.child("locations");
    return query.once('value')
      .then((snapshot) => {
        snapshot.forEach(function(item) {
          var location = {
            key: item.key,
            name: item.val().name,
            address: item.val().address,
            lat: item.val().local_lat_centroid,
            lng: item.val().local_long_centroid,
            weekend_rate: item.val().weekend_rate,
            green_fee: item.val().green_fee
          };
          locations.push(location);          
        });        
        return Promise.resolve(locations);
      }).catch((err) => {        
        console.log("err=", err);
        return Promise.resolve(locations);;
      });
  }

  fetchAllCourses() {
    let courses = [];
    const dbRef = firebase.database().ref();

    let query: any = dbRef.child("locations");
    return query.once('value').then((snapshot) => {        
      snapshot.forEach(function(item) {
        var course = {
          key: item.key,
          imageUrl: "",
          name: item.val().name,
          address: item.val().address,
          phone: item.val().phone,
          metal_spikes: item.val().metal_spikes,
          play_five: item.val().play_five,
          holes: item.val().holes,
          type: item.val().type,
          year_built: item.val().year_built,
          season: item.val().season,
          designer: item.val().designer,
          guest_policy: item.val().guest_policy,
          dress_code: item.val().dress_code,
          green_fee: item.val().green_fee,
          weekend_rate: item.val().weekend_rate,
          adv_fee: item.val().adv_fee
        };
        courses.push(course);
      });
      return Promise.resolve(courses);
    }).catch((err) => {        
      console.log("err=", err);
      return Promise.resolve(courses);;
    });
  }  
}