import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { User } from '../../models/user';
import { Storage } from '@ionic/storage';

import firebase from "firebase";
import $ from "jquery"; 
import 'intl-tel-input';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  user: User;
  username: string;
  public initialized:boolean = false;
  public isDisabled:boolean = true;

  img='assets/images/default.png';

  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, private camera: Camera, private storage: Storage ) {
    this.storage.get('current_user').then((val)=> {
      this.user = val;
      console.log(this.user);
      this.username = this.user.firstname + " " + this.user.lastname;
      this.initialized = true;
    });
  }

  changeData(input){
    this.isDisabled =! this.isDisabled; 
  }
  
  ngOnInit(): any {
    let telInput = $("#elemtId");
    let output = $("#output");

    telInput.intlTelInput();
    // listen to "keyup", but also "change" to update when the user selects a country
    telInput.on("keyup change", function() {
      var intlNumber = telInput.intlTelInput("getNumber");
      console.log(intlNumber);
      if (intlNumber) {
        output.text("International: " + intlNumber);
      } else {
        output.text("Please enter a number below");
      }
    });
  }


  getPhoto(){
      var buttons=[
        {
          text: 'Camera',
          handler: () => {
            this.get_camera(2);
          }
        },{
          text: 'Gallery',
          handler: () => {
              this.get_camera(1);
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }

      ]
      if(this.img!='assets/images/default.png'){ 
        let butttondelete={text:'Delete',handler:()=>{this.delete_image()}}
        buttons.push(butttondelete) 
      }
      this.actionSheetCtrl.create({buttons: buttons}).present();
  }

  delete_image(){
      this.img='assets/images/default.png';
  } 

  get_camera=function (source) {
    const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        allowEdit:true,
        targetWidth:80,
        targetHeight:80,
        correctOrientation:true
    }

  if(source==1)options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY
  else options.sourceType= this.camera.PictureSourceType.CAMERA

  this.camera.getPicture(options).then((imageData) => {
    console.log(imageData)
        this.img='data:image/jpeg;base64,' + imageData;
        this.user.picture='data:image/jpeg;base64,' + imageData;
  }, (err) => { });
  } 

  showToast(msg) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  save() {
    firebase.database().ref('users/' + this.user.uid).update(this.user).then(()=>{
      this.isDisabled =! this.isDisabled; 
      this.showToast('Profile is updated!');
    });
  }
}
