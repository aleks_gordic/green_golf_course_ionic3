import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import firebase from "firebase";
import {Location} from '../../models/location';
import { GlobalProvider } from "../../providers/global";

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {
  list: Array<Location> = [];
  filters: Array<Location> = [];
  loadingView: any;
  searchQuery: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,public alertCtrl: AlertController, public global: GlobalProvider) {
    this.list = [];
    this.filters = [];
    if (this.global.features.length == 0) {
      this.fetchAllLocations().then((locations) => {
        for (var i = 0; i < 10; i ++) {
          this.global.features.push({
            lat: locations[i].lat,
            lng: locations[i].lng,
            name: locations[i].name,
            price: locations[i].green_fee
          });
        }
        this.list = this.global.features;
        this.filters = this.global.features;
      });
    } else {
      this.list = this.global.features;
      this.filters = this.global.features;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListPage');    
  }

  showLoadingView(show = true, desc?: String) {
    if (show) {
      // show showLoading view
      this.loadingView = this.loadingCtrl.create();
      this.loadingView.present();
    }
    else {
      if (this.loadingView) {
        // hide
        this.loadingView.dismiss();
      }
    }
  }
  // search function
  searchLocations() {
    this.filters = [];
    for (var i = 0; i < 10; i ++) {
      if (this.list[i].name.toLowerCase().match(this.searchQuery.toLowerCase())) {
        this.filters.push(this.list[i]);
      }
    }
  }
  /**
   * get all locations
   */
  fetchAllLocations() {
    
    let locations = [];
    
    // fetch locations
    const dbRef = firebase.database().ref();

    let query: any = dbRef.child("locations");
    return query.once('value')
      .then((snapshot) => {
        
        snapshot.forEach(function(item) {
          var location = {
            key: item.key,
            name: item.val().name,
            address: item.val().address,
            lat: item.val().local_lat_centroid,
            lng: item.val().local_long_centroid,
            weekend_rate: item.val().weekend_rate,
            green_fee: item.val().green_fee
          };
          locations.push(location);
        });        
        return Promise.resolve(locations);
      }).catch((err) => {        
        console.log("err=", err);
        return Promise.resolve(locations);;
      });
  } 

  itemTapped(event, item) {
    this.global.courses.forEach(course => {
      if (course.name == item.name) {
        this.navCtrl.push('DetailsPage',{ course:course });
      }
    });
  }
}
