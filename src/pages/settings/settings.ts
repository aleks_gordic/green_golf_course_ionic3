import { Component } from '@angular/core';
import { IonicPage, NavController, App  } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AngularFireAuth } from 'angularfire2/auth';
import { GlobalProvider } from "../../providers/global";

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public app: App, private storage: Storage, private fbauth: AngularFireAuth, public global: GlobalProvider) {    
  }
  openprofile(){
    this.navCtrl.parent.select(3);
  }
  logout(){
    this.fbauth.auth.signOut();
    this.storage.set('current_user', null);
    this.global.features = null;
    this.app.getRootNav().setRoot('LoginPage');
  }
  
}
