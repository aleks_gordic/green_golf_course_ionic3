export interface Course {
  key?: string;
  imageUrl: string;
  name: string;
  address: string;
  phone: string;
  metal_spikes: string;
  play_five: string;
  holes: string;
  type: string;
  year_built: string;
  season: string;
  designer: string;
  guest_policy: string;
  dress_code: string;
  green_fee: string;
  weekend_rate: string;
  adv_fee: string;
}
