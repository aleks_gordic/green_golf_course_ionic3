import { Injectable } from '@angular/core';
import {User} from '../models/user';
import {Location} from '../models/location';
import {Course} from '../models/course';

@Injectable()
export class GlobalProvider {
  public current_user: User;
  public features: Array<Location> = [];
  public courses: Array<Course> = [];
}
